<div class="Box-sc-g0xbh4-0 bJMeLZ js-snippet-clipboard-copy-unpositioned" data-hpc="true"><article class="markdown-body entry-content container-lg" itemprop="text"><p dir="auto"><a href="https://www.python.org/downloads/release/python-380/" rel="nofollow"><img src="https://camo.githubusercontent.com/23c0510e5398a8357e56ed89806c744e48a79d6e46ce2a9ed875f1d482440c1e/68747470733a2f2f696d672e736869656c64732e696f2f62616467652f707974686f6e2d332e382d626c7565" alt="Python版本" data-canonical-src="https://img.shields.io/badge/python-3.8-blue" style="max-width: 100%;"></a></p>
<div class="markdown-heading" dir="auto"><h1 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">维萨里</font></font></h1><a id="user-content-invesalius" class="anchor" aria-label="永久链接：InVesalius" href="#invesalius"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">InVesalius 根据使用 CT 或 MRI 设备获取的一系列 2D DICOM 文件生成 3D 医学成像重建。InVesalius 已国际化（目前提供英语、葡萄牙语、法语、德语、西班牙语、加泰罗尼亚语、罗马尼亚语、韩语、意大利语和捷克语）、多平台（GNU Linux、Windows 和 MacOS）并提供多种工具：</font></font></p>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">DICOM 支持包括：(a) ACR-NEMA 版本 1 和 2；(b) DICOM 版本 3.0（包括 JPEG 的各种编码 - 无损和有损 -、RLE）</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">支持分析文件</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">支持 BMP、PNG、JPEG 和 TIF 文件</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">图像处理功能（缩放、平移、旋转、亮度/对比度等）</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">基于二维切片的分割</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">根据感兴趣的组织预定义阈值范围</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">基于分水岭的分割</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">基于 2D 切片的编辑工具（类似于画笔）</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">线性和角度测量工具</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">体积重定向工具</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">3D 表面创建</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">3D表面体积测量</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">3D 表面连通性工具</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">3D 表面导出（包括：二进制和 ASCII STL、PLY、OBJ、VRML、Inventor）</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">高品质体积渲染投影</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">预定义体积渲染预设</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">体积渲染裁剪平面</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">图片导出（包括：BMP、TIFF、JPG、PostScript、POV-Ray）</font></font></li>
</ul>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">发展</font></font></h3><a id="user-content-development" class="anchor" aria-label="固定链接：开发" href="#development"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<ul dir="auto">
<li><a href="https://github.com/invesalius/invesalius3/wiki/Running-InVesalius-3-in-Linux"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在 Linux 中运行 InVesalius 3</font></font></a></li>
<li><a href="https://github.com/invesalius/invesalius3/wiki/Running-InVesalius-3-in-Mac"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在 Mac 中运行 InVesalius 3</font></font></a></li>
<li><a href="https://github.com/invesalius/invesalius3/wiki/Running-InVesalius-3-in-Windows"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在 Windows 中运行 InVesalius 3</font></font></a></li>
</ul>
</article></div>
